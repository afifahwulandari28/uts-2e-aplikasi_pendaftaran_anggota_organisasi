package wulandari.fifah.uts




import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class Db(context:Context): SQLiteOpenHelper(context,Db_name,null,Db_ver) {

    companion object{
        val Db_name = " dartar"
        val Db_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tbljur = "create table jurusan(id_jurusan integer primary key autoincrement , nama_jurusan text not null )"
        val tblmhs = "create table mhs(nim text not null, nama_mhs text not null, alamat text not null, nohp text not null , id_jurusan text not null)"
        val inserjur = "insert into jurusan('nama_jurusan') values ('MI'),('ME'),('AK')"
        val insert = "insert into mhs('nim','nama_mhs','alamat','nohp','id_jurusan') values ('123113231','diasssd','dsaa3231s','09231865','2')"
        db?.execSQL(tbljur)
        db?.execSQL(tblmhs)
        db?.execSQL(inserjur)
        db?.execSQL(insert)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}