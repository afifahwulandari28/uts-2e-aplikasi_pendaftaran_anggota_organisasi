package wulandari.fifah.uts

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color

import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.FragmentContainer
import androidx.fragment.app.FragmentTransaction
//import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(){


    //lateinit var fragMHs: fragMHs
    //lateinit var fragprod: fragprod
    lateinit var ft : FragmentTransaction
    lateinit var db : SQLiteDatabase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //fragMHs = fragMHs()
        //fragprod = fragprod()
       // bottomNavigationView.setOnNavigationItemSelectedListener(this)
        db = Db(this).writableDatabase

    }

    fun getDB() : SQLiteDatabase{
        return db
    }

    fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi->{
                ft= supportFragmentManager.beginTransaction()
                //ft.replace(R.id.frameLayout,fragprod).commit()
                frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
                frameLayout.visibility = View.VISIBLE

            }
            R.id.itemMhs->{
                ft= supportFragmentManager.beginTransaction()
                //ft.replace(R.id.frameLayout,fragMHs).commit()
                frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout->frameLayout.visibility = View.GONE
        }

        return true
    }


}
